@extends('layouts.app')
@section('content')

       <h1>All Project</h1>

       <a href="{{route('projectCreate')}}" class="btn btn-primary btn-lg active btn-sm mb-2" role="button" aria-pressed="true">Create New Project</a>


    <div class="table-responsive">
        <table class="table table-bordered " id="dataTable" width="100%" cellspacing="0">
            <thead>
            <tr>
                <th>Title</th>
                <th>description</th>
                <th>Created</th>
                <th>Updated</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
            </thead>

            <tbody>

            @foreach($projects as $project)
                <tr>
                    <td>{{$project->title}}</td>
                    <td>{{$project->description}}</td>
                    <td>{{$project->created_at ? $project->created_at->diffForHumans() : ''}}</td>
                    <td>{{$project->updated_at ? $project->updated_at->diffForHumans() : ''}}</td>
                    <td><a href="{{route('projectEdit', $project->id)}}">Edit</a></td>
                    <td>
                        <a href="#">
                            {!! Form::open(['method'=>'POST','action'=>['ProjectsController@destroy',$project->id]]) !!}

                            {!! Form::submit('Delete',['class'=>'btn btn-danger btn-sm', 'onclick' => 'return confirm("Are you sure you wont do delete")']) !!}
                            {!! Form::close() !!}
                        </a>
                    </td>
                </tr>
            @endforeach

            </tbody>
        </table>

    </div>


@endsection
