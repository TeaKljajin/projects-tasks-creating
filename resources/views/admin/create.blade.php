@extends('layouts.app')
@section('content')

    <h1 id="createProject1" class="mb-4">Create Project</h1>
    <div  id="projectCreate">
        {!! Form::open(['method'=>'POST','action'=>'ProjectsController@store']) !!}
        <div class="form-group">
            {!! Form::label('title','Title:') !!}
            {!! Form::text('title', null, ['class'=>'form-control']) !!}
        </div>
        <div class="form-froup">
            {!! Form::label('description','Description:') !!}
            {!! Form::textarea('description', null, ['class'=>'form-control', 'rows'=>4, 'cols'=> 8]) !!}
        </div>
        <br>

        <div class="row">
            <div class="col-10">
                <div class="form-group">
                    {!! Form::submit('Create Project', ['class'=>'btn btn-primary btn-sm']) !!}
                </div>
            </div>
            <div class="col-2">
                <a href="{{route('projectIndex')}}" class="btn btn-primary btn-lg active btn-sm ml-4" role="button" aria-pressed="true">Go Back</a>

            </div>
            {!! Form::close() !!}
        </div>
    </div>

@endsection
