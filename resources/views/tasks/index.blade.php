@extends('layouts.app')
@section('content')

    <h1>All Tasks By {{Auth::user()->name}}</h1>

    <a href="{{route('createTask')}}" class="btn btn-primary btn-lg active btn-sm mb-2" role="button" aria-pressed="true">Create New Task</a>


    <div class="table-responsive">
        <table class="table table-bordered " id="dataTable" width="100%" cellspacing="0">
            <thead>
            <tr>
                <th>Name</th>
                <th>Priority</th>
                <th>Project_id</th>
                <th>Created</th>
                <th>Updated</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
            </thead>

            <tbody>

            @foreach($tasks as $task)
                <tr>
                    <td>{{$task->name}}</td>
                    <td>{{$task->priority}}</td>
                    <td>{{$task->project_id}}</td>
                    <td>{{$task->created_at ? $task->created_at->diffForHumans() : ''}}</td>
                    <td>{{$task->updated_at ? $task->updated_at->diffForHumans() : ''}}</td>
                    <td><a href="{{route('editTask', $task->id)}}">Edit</a></td>
                    <td>
                        <a href="#">
                            {!! Form::open(['method'=>'POST','action'=>['TasksController@destroy',$task->id]]) !!}

                            {!! Form::submit('Delete',['class'=>'btn btn-danger btn-sm', 'onclick' => 'return confirm("Are you sure you wont do delete")']) !!}
                            {!! Form::close() !!}
                        </a>
                    </td>
                </tr>
            @endforeach

            </tbody>
        </table>

    </div>


@endsection

