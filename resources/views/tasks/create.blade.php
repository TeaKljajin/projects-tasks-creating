@extends('layouts.app')
@section('content')

    <h1 id="createProject1" class="mb-4">Create Task</h1>
    <div  id="projectCreate">
        {!! Form::open(['method'=>'POST','action'=>'TasksController@store']) !!}
        {!! Form::hidden('user_id', Auth::user()->id) !!}
        <div class="form-froup">
            {!! Form::label('name','Name:') !!}
            {!! Form::text('name', null, ['class'=>'form-control']) !!}

        </div>
        <br>
        <div class="form-froup">
            {!! Form::label('priority','Priority:') !!}
            {!! Form::number('priority', null, ['class'=>'form-control']) !!}
        </div>
        <br>
        <div class="form-group">
            {!! Form::label('project_id','Choose Project title:') !!}
            {!! Form::select('project_id', $projects, null, ['class'=>'form-control']) !!}
        </div>
        <br>
        <div class="row">
            <div class="col-10">
                <div class="form-group">
                    {!! Form::submit('Create Task', ['class'=>'btn btn-primary btn-sm']) !!}
                </div>
            </div>
            <div class="col-2">
                <a href="{{route('indexTask')}}" class="btn btn-primary btn-lg active btn-sm ml-4" role="button" aria-pressed="true">Go Back</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>

@endsection
