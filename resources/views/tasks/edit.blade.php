@extends('layouts.app')
@section('content')

    <h1 id="createProject1" class="mb-4">Edit Task</h1>
    <div  id="projectCreate">
        {!! Form::model($task,['method'=>'POST','action'=>['TasksController@update', $task->id]]) !!}
        <div class="form-group">
            {!! Form::label('name','Name:') !!}
            {!! Form::text('name', null, ['class'=>'form-control']) !!}
        </div>
        <br>
        <div class="form-group">
            {!! Form::label('priority','Priority:') !!}
            {!! Form::number('priority', null, ['class'=>'form-control']) !!}
        </div>

            {!! Form::hidden('project_id', $task->project_id) !!}


        <br>
        <div class="row">
            <div class="col-10">
                <div class="form-group">
                    {!! Form::submit('Update Task', ['class'=>'btn btn-primary btn-sm']) !!}
                </div>
            </div>
            <div class="col-2">
                <a href="#" class="btn btn-primary btn-lg active btn-sm ml-4" role="button" aria-pressed="true">Go Back</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>

@endsection

