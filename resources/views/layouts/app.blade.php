@include('includes.header')
<body>
    <div id="app">
       @include('includes.topNavBar')
     <div class="container">
         <main class="py-4">
            @include('includes.messages')
             @include('includes.errors')
             @yield('content')

         </main>

     </div>

    </div>

 @include('includes.footer')
</body>
</html>
