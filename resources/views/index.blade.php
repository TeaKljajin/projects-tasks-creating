@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-lg-6">
             <h1>All Projects by Codetest</h1>
            @foreach($alProjects as $alProject)
            <h4>{{$alProject->title}}</h4>
            <p>{{$alProject->description}}</p>
            @endforeach



        </div>
        <div class="col-lg-6">
           <h3>Select a project and see all tasks by that project</h3>
            {!! Form::open(['method'=>'POST','action'=>'PublicsController@showTasks']) !!}
            <div class="form-froup">
                {!! Form::label('project_id','Choose Project title:') !!}
                {!! Form::select('project_id', $projects, null, ['class'=>'form-control']) !!}
            </div>
            <br>
            <div class="form-group">
                {!! Form::submit('Submit', ['class'=>'btn btn-primary btn-sm']) !!}
            </div>
            {!! Form::close() !!}

        </div>
    </div>
@endsection
