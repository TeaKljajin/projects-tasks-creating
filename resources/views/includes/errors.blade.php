
<div id="erro">
    @if(count($errors) > 0)
        @foreach($errors->all() as $error)
            <div class="btn btn-danger col-sm-offset-2 mt-3">
                {{$error}}
            </div>

        @endforeach
    @endif
</div>

