<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use App\Task;

class PublicsController extends Controller
{
    public function index(){
        $alProjects = Project::all();
        $projects = Project::pluck('title', 'id')->all();
        return view('index', compact('projects', 'alProjects'));
    }

    public function showTasks(Request $request){
        $this->validate($request,[
            'project_id'=>'required'
        ]);
        $alProjects = Project::all();
        $projects = Project::pluck('title', 'id')->all();
        $project_id = $request->input('project_id');
        $tasks = Task::where('project_id',$project_id)->orderBy('priority', 'Asc')->get();
        return view('showTasks', compact('tasks','project_id', 'projects','alProjects'));
    }









}
