<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use Illuminate\Support\Facades\Auth;
use App\Http\Middleware\CheckRole;


class ProjectsController extends Controller
{
    public function __construct()
    {
        $this->middleware('checkRole:1');
    }





    public function index(){
        $projects = Project::all();
        return view('admin.index', compact('projects'));
    }

    public function create(){
        return view('admin.create');
    }

    public function store(Request $request){
        $this->validate($request,[
            'title'=>'required|string|max:255',
            'description'=>'required'
        ]);
        $input = $request->all();
        Project::create($input);
        return redirect('/projects')->with('success', 'Project has been created successfully');

    }

    public function edit($id){
        $project = Project::findOrFail($id);
         return view('admin.edit', compact('project'));
    }

    public function update(Request $request, $id){
        $project = Project::find($id);
        $this->validate($request,[
            'title'=>'required|string|max:255',
            'description'=>'required'
        ]);
        $input = $request->all();
        $project->update($input);
        return redirect('/projects')->with('success', 'Project has been updated successfully');
    }

    public function destroy($id){
       $project = Project::findOrFail($id);
       $project->delete();
       return redirect('projects')->with('success', 'Project has been deleted successfully');
    }
}
