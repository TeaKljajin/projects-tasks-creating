<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use App\Task;
use Illuminate\Support\Facades\Auth;

class TasksController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $tasks = Task::where('user_id', Auth::user()->id)->get();
        return view('tasks.index', compact('tasks'));

    }
    public function create(){
        $projects = Project::pluck('title', 'id')->all();
        return view('tasks.create', compact('projects'));
    }


    public function store(Request $request){
        $this->validate($request,[
            'name'=>'required|string|max:50',
            'priority'=>'required|integer|min:1|max:10',
            'project_id'=>'required'
        ]);
        $input = $request->all();
        Task::create($input);
        return redirect('/dashboard')->with('success', 'Task has been created successfully');

    }



    public function edit($id){
        $task = Task::findOrFail($id);
          return view('tasks.edit', compact('projects', 'task'));
    }

    public function update(Request $request,$id){
        $task = Task::findOrFail($id);
        $this->validate($request,[
            'name'=>'required|string|max:50',
            'priority'=>'required|integer|min:1|max:10',
        ]);
        $input = $request->all();
        $task->update($input);
        return redirect('/dashboard')->with('success', 'Task has been updated successfully');

    }

    public function destroy($id){
        $task = Task::findOrFail($id);
        $task->delete();
        return redirect('/dashboard')->with('success', 'Task has been deleted successfully');
    }
}
