<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PublicsController@index')->name('main');
Route::post('/', 'PublicsController@showTasks')->name('showTasks');

Auth::routes();

Route::get('/dashboard', 'TasksController@index')->name('indexTask');
Route::get('/dashboard/create', 'TasksController@create')->name('createTask');
Route::post('/dashboard/store', 'TasksController@store')->name('storeTask');
Route::get('/dashboard/{id}/edit', 'TasksController@edit')->name('editTask');
Route::post('/dashboard/{id}/update', 'TasksController@update')->name('updateTask');
Route::post('/dashboard/{id}/destroy', 'TasksController@destroy')->name('destroyTask');

Route::get('/projects', 'ProjectsController@index')->name('projectIndex');
Route::get('/project/create', 'ProjectsController@create')->name('projectCreate');
Route::post('/project/store', 'ProjectsController@store')->name('projectStore');
Route::get('/project/{id}/edit', 'ProjectsController@edit')->name('projectEdit');
Route::post('/project/{id}/update', 'ProjectsController@update')->name('projectUpdate');
Route::post('/projects/{id}/destroy', 'ProjectsController@destroy')->name('projectDestroy');


